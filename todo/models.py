from django.db import models

class TaskList(models.Model):
	name = models.CharField(max_length=100)
	userId = models.IntegerField()

	def __unicode__(self):
		return self.name

class Task(models.Model):
	title = models.CharField(max_length=100)
	description = models.CharField(max_length=200, default="")
	completed = models.BooleanField(default=False)
	taskList = models.ForeignKey(TaskList, related_name='tasks', to_field='id')

	def __unicode__(self):
		return self.title

# Change 2
