from django.conf.urls import patterns, include, url
import settings
from django.contrib.auth.views import login, logout


from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'todo.views.todo', name='todo'),
    url(r'^api/', include('todo_api.urls')),
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    (r'^accounts/', include('registration.backends.simple.urls')),
    (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root':settings.MEDIA_ROOT}),
    (r'^js/(?P<path>.*)$', 'django.views.static.serve', {'document_root':settings.MEDIA_ROOT})
)

urlpatterns += patterns('',
	url(r'^login/$', login, kwargs={'template_name': 'login.html'}, name="login_page"),
	url(r'^logout/$', logout, kwargs={'next_page': '/'}, name="logout_page")
	)