define([],
	function() {
		var Task = Backbone.Model.extend({
			urlRoot : "api/tasks/"
		});
		return Task

	});