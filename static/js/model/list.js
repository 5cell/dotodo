define([],
	function() {
		var TaskList = Backbone.Model.extend({
			urlRoot : 'api/tasklists/'
		});
		
		return TaskList

	});