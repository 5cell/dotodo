define(['model/task'],
	function(Task) {
		var Tasks= Backbone.Collection.extend({
			model : Task,
			initialize : function(models, options){
				this.listId = options.listId;
				//this.fetch();
				//console.log(this.listId);
			},
			url : function() {
				return 'api/tasks/'+this.listId+'/'
			}
		});
		
		return Tasks

	});