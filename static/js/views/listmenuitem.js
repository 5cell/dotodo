define(['text!templates/listmenuitem.html', 'views/task/taskspanel'],
	function(listMenuItemTemplate, TasksPanelView) {
		var ListMenuItemView = Backbone.View.extend({
			template : _.template(listMenuItemTemplate),
			tagName : 'li',
			//className : "list-menu-item",
			events : {
				'click' : 'openTasks'
			},

			initialize : function() {
				this.model.on("change", this.render, this);
				this.model.on("destroy", this.remove, this);
			},
			openTasks: function() {
				if(!dotodo.activeMenuItem) {
					dotodo.activeMenuItem = this;
				}
				else {
					dotodo.activeMenuItem.$el.removeClass('active');
					dotodo.activeMenuItem = this;
				}
				this.$el.addClass('active');

				// this.model.on("change", this.render, this);
				// this.model.on("destroy", this.remove, this);
				var tasksPanel = new TasksPanelView({model:this.model});
				tasksPanel.render();
			},

			render : function() {
				//console.log(this.model.get("name"));
				this.$el.html(this.template({'name': this.model.get("name")}));
				return this;
			}
		});
		
		return ListMenuItemView

	});