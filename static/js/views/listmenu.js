define(['views/listmenuitem'],
	function(MenuItemView) {
		var ListMenu= Backbone.View.extend({
			el : "#tasklistpanel",
			//className: 'nav nav-pills nav-tasked',

			initialize : function () {
				this.collection.on('add', this.renderMenuItem, this);
				this.collection.fetch();
				
			},

			renderMenuItem : function(model) {
				var item = new MenuItemView({model: model});
				//dotodo.activeMenuItem = item;
				item.openTasks();
				this.$el.prepend(item.render().el);
			},

			render : function() {
				// var s = this;
				// this.collection.fetch({reset:true, success: function() {
				// 	s.collection.each(function(model) {
				// 		console.log(s.renderMenuItem(model).el);
				// 		s.$el.append(s.renderMenuItem(model).el);
				// 		//s.renderMenuItem(model);
						
				// 	});

				// }})
				// console.log(this.$el);

				// return this;
				return this;
			}

		});
		
		return ListMenu

	});