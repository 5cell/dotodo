define(['text!templates/tasklistheader.html'],
	function(taskListHeaderTemplate) {
		var TaskListHeaderView = Backbone.View.extend({
	//		el : "#tasklist-header",
			className : "",
			template : _.template(taskListHeaderTemplate),
			events : {
				'click #tasklist-edit-btn' : 'editTaskListName',
				'click #tasklist-delete-btn': 'deleteTaskList',
				'click #listedit-btn': 'updateListName',
				'click #edit-cancel-btn' : 'cancelEdit',
				'click #addtask-btn' : 'activateTaskAddForm'
			},

			initialize : function() {
				this.model.on("change", this.render, this);
				
			},

			initTaskAddForm : function(taskAddForm) {
				this.taskAddForm = taskAddForm;
				this.taskAddForm.$el.hide();
			},

			updateListName : function() {
				var $name = this.$el.find("input[name=name]");
				var s = this;
				this.model.save({name:$name.val()}, {
					success : function() {
						s.$el.find("input[name=name]").val("")
						s.$el.find('#header-display').show();
						s.$el.find('#edit-name').hide();
					}
				});
			},

			activateTaskAddForm : function() {
				this.taskAddForm.$el.show();
			},

			editTaskListName : function() {
				this.$el.find('#header-display').hide();
				this.$el.find('#edit-name').show();
			},

			cancelEdit  : function() {
				this.$el.find('#header-display').show();
				this.$el.find('#edit-name').hide();
			},

			deleteTaskList : function() {
				this.model.destroy();

			},

			render : function() {
				this.$el.html(this.template({'listName': this.model.get('name')}));
				this.$el.find("#edit-name").hide();
				return this;
			}
		});
		return TaskListHeaderView
	});