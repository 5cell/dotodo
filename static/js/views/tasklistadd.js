define(['text!templates/tasklistadd.html'],
	function(taskListAdd) {
		var TaskListAddView = Backbone.View.extend({
			el : "#tasklist-add",
			template : _.template(taskListAdd),
			events : {
				'click #tasklist-add-btn' : 'addNewList'
			},

			addNewList : function() {
				var newListName = this.$el.find("input[name=listname]").val();
				this.collection.create({name: newListName});
				this.render();
			},

			render : function() {
				this.$el.html(this.template());
				return this;
			}
		});

		return TaskListAddView

	});