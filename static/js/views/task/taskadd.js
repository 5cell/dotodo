define(['text!templates/task/taskadd.html'],
	function(taskAddTemplate) {
		var TaskAddView = Backbone.View.extend({
	//		el : "#taskadd",
			className : "",
			template : _.template(taskAddTemplate),

			events : {
				//'click #taskadd-btn' : 'activeAddForm',
				'click #cancel-taskadd' : 'cancelAdd',
				'click #addtask' : 'submit'
			},
			
			submit : function () {
				var $title = this.$el.find("input[name=title]"),
					desc = this.$el.find("input[name=description]").val();
				this.collection.create({title: $title.val(), description : desc, taskList_id : this.collection.listId});
				//this.$el.find('#taskadd-btn').show();
				//this.$el.find('#taskform-btn').hide();

				return this.cancelAdd();;
			},

			cancelAdd : function() {
				this.$el.find("input[name=title]").val("");
				this.$el.find("input[name=description]").val("");
				this.$el.hide();
				return false;
			},


			render : function() {
				this.$el.html(this.template());
				return this;
			}

		});

		return TaskAddView
	});