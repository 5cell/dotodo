define(['text!templates/task/task.html','text!templates/task/edittask.html'],
	function(taskTemplate, editTaskTemplate) {

		var TaskView = Backbone.View.extend({
			taskTemplate : _.template(taskTemplate),
			editTemplate : _.template(editTaskTemplate),
			className : "",
			events : {
				'click #edit-btn' : 'editForm',
				'click #edit-complete' : 'updateModel',
				'click #edit-cancel' : 'editCancel',
				'click #delete-btn' : 'destroy',
				'click #done' : 'toggleCompleted'
			},

			initialize : function() {
				this.model.on("change", this.render, this);
				this.model.on("destroy", this.remove, this);
			},

			updateModel : function() {
				var newTitle = this.$el.find("input[name=title]").val(),
					newDesc = this.$el.find("input[name=desc]").val();
					this.model.save({'title':newTitle, 'description': newDesc}, {
						success : function() {
							this.render();
						}
					});
			},

			toggleCompleted : function(){
					var s = this;
					this.model.save({'completed' : !this.model.get('completed')}, {
						success : function() {
							s.render();
						}
					});
			},

			editCancel : function() {
				this.$el.empty();
				var x = this.model.toJSON();
				if(this.model.get('completed')) {
					x['completed'] = 'checked';
				}
				else {
					x['completed'] = '';
				}
				this.$el.html(this.taskTemplate(x));
			},

			editForm : function() {
				this.$el.empty();
				this.$el.html(this.editTemplate(this.model.toJSON()));
			},

			render : function() {
				var x = this.model.toJSON();
				if(this.model.get('completed')) {

					x['completed'] = 'checked';
				}
				else {
					x['completed'] = '';
				}
				this.$el.html(this.taskTemplate(x));
				return this;

			},

			destroy : function() {
				this.model.destroy();
				return false;
			}

		});

		return TaskView;
	});