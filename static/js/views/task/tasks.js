define(['views/task/task'],
	function(TaskView) {
		var TasksView = Backbone.View.extend({
			//tagName : 'ul'

			initialize : function() {
				this.collection.on('add', this.renderTask, this);
				this.collection.fetch();
				//console.log("hello");
			},

			renderTask : function(model) {
				var task = new TaskView({model : model});
				this.$el.prepend(task.render().el);
			},

			render : function() {
				return this;
			}

		});

		return TasksView;

	});