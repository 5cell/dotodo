define(['views/task/taskadd', 'views/tasklistheader','views/task/tasks', 'collection/tasks'],
	function(TaskAddView, TaskListHeaderView, TasksView, Tasks) {
		var TaskPanelView = Backbone.View.extend({
			el : "#central-container",
			className : "",

			initialize : function() {
				this.tasks = new Tasks([],{listId : this.model.id});
				this.taskAddView = new TaskAddView({collection : this.tasks});
				this.tasklistHeader = new TaskListHeaderView({model:this.model});
				this.tasklistHeader.initTaskAddForm(this.taskAddView);
				this.tasksView = new TasksView({collection : this.tasks});
				this.model.on("destroy", this.remove, this);

			},

			render : function(){
				this.$el.empty();
				this.$el.append(this.tasklistHeader.render().el);
				this.$el.append(this.taskAddView.render().el);
				this.$el.append(this.tasksView.render().el);
				//console.log(this.$el.html());
				return this;

			}
		});
		return TaskPanelView;
	});