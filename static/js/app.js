define(['views/listmenu', 'collection/lists', 'views/tasklistadd'],
	function (MenuView, TaskLists, TaskListAdd) {

		var App = function() {
			var tasklists = new TaskLists();
			this.views.listaddview = new TaskListAdd({collection : tasklists });
			this.views.listaddview.render();
			this.views.listview = new MenuView({collection : tasklists});
			this.views.listview.render();
			//this.views.listview.collection.fetch();
			
		};

		App.prototype = {
			views : {}
		};


		return App;

	});