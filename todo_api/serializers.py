from rest_framework import serializers
from todo.models import Task, TaskList

class TaskSerializer(serializers.ModelSerializer):

	description = serializers.CharField(required=False)
	completed = serializers.BooleanField(required=False)
	taskList_id = serializers.IntegerField(required=False)

	def restore_object(self, attrs, instance=None):
		if instance:
			instance.title = attrs.get('title', instance.title)
			instance.description = attrs.get('description', instance.description)
			instance.completed = attrs.get('completed', instance.completed)
			instance.taskList_id = attrs.get('taskList_id', instance.taskList_id)
			return instance
		return Task(**attrs)

	class Meta:
		model = Task
		fields = ('id', 'title', 'description', 'completed', 'taskList_id')

class TaskListSerializer(serializers.ModelSerializer):
	tasks = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
	userId = serializers.IntegerField(required=False)
	
	def restore_object(self, attrs, instance=None):
		if instance:
			instance.name = attrs.get('name', instance.name)
			instance.userId = attrs.get('userId', instance.userId)
			return instance
		return TaskList(**attrs)
		
	class Meta:
		model = TaskList
		fields = ('id','name', 'userId')
