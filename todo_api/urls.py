from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
    url(r'^tasklists/(?P<pk>[0-9]*)(/?)$', 'todo_api.views.tasklists', name='tasklists'),
    #url(r'^tasklist/(?P<pk>[0-9]+)/$', 'todo_api.views.task_list', name='tasklist'),
    url(r'^tasks/(?P<pk>[0-9]*)(/?)$', 'todo_api.views.tasks', name='tasks'),
)
