from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from todo.models import Task, TaskList
from todo_api.serializers import TaskSerializer, TaskListSerializer
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from django.contrib.auth.decorators import login_required


class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


@login_required
@csrf_exempt
def tasklists(request, pk):
	if request.is_ajax():
		if request.method == 'GET':
			lists = TaskList.objects.filter(userId=request.user.id)
			serializer = TaskListSerializer(lists, many=True)
			return JSONResponse(serializer.data)
		elif request.method == 'POST':
			print "hiii"
			data = JSONParser().parse(request)
			data['userId'] = request.user.id
			serializer = TaskListSerializer(data=data)
			if serializer.is_valid():
				serializer.save()
				return JSONResponse(serializer.data, status=201)
			else:
				return JSONResponse(serializer.errors, status=400)
		elif request.method == 'PUT':
			tasklist = TaskList.objects.get(id=pk)
			data = JSONParser().parse(request)
			serializer = TaskListSerializer(tasklist, data=data)
			if serializer.is_valid():
				serializer.save()
				return JSONResponse(serializer.data, status=201)
			else:
				return JSONResponse(serializer.errors, status=400)
		elif request.method == 'DELETE':
			l = TaskList.objects.get(id=pk)
			l.delete()
			return HttpResponse(status=204)




@login_required
@csrf_exempt
def tasks(request, pk):
	if request.method == 'GET':
		lists = Task.objects.filter(taskList__id__exact=pk)
		serializer = TaskSerializer(lists, many=True)
		return JSONResponse(serializer.data)
	if request.method == 'POST':
		data = JSONParser().parse(request)
		serializer = TaskSerializer(data=data)
		if serializer.is_valid():
			serializer.save()
			return JSONResponse(serializer.data, status=201)
		else: return JSONResponse(serializer.errors, status=400)
	elif request.method == 'PUT':
		data = JSONParser().parse(request)
		task = Task.objects.get(id=pk)
		serializer = TaskSerializer(task,data=data)
		if serializer.is_valid():
			serializer.save()
			return JSONResponse(serializer.data, status=201)
		else:
			return JSONResponse(serializer.errors, status=400)
	elif request.method == 'DELETE':
		task = Task.objects.get(id=pk)
		task.delete()
		return HttpResponse(status=204)
